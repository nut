<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de">
<defaultcodec></defaultcodec>
<context>
    <name>@default</name>
    <message>
        <location filename="constants.h" line="7"/>
        <source>QNUT - Qt client for Network UTility Server (NUTS)</source>
        <translation>QNUT - Qt Client für den Network UTility Server (NUTS)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../libnutwireless/types.cpp" line="252"/>
        <source>UNDEFINED</source>
        <translation>UNDEFINIERT</translation>
    </message>
</context>
<context>
    <name>adhoc</name>
    <message>
        <location filename="adhoc.ui" line="13"/>
        <source>Ad-hoc configuration</source>
        <translation>Ad-hoc Konfiguration</translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="19"/>
        <source>SSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="41"/>
        <source>hex. chars</source>
        <translation>hex. Zeichen</translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="50"/>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="80"/>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="103"/>
        <source>TKIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="108"/>
        <source>CCMP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="153"/>
        <source>leave unchanged</source>
        <translation>unverändert lassen</translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="93"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="98"/>
        <source>WEP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="adhoc.ui" line="131"/>
        <source>(Pre Shared) Key</source>
        <translation>Schlüssel (PSK)</translation>
    </message>
</context>
<context>
    <name>airset</name>
    <message>
        <location filename="airset.ui" line="59"/>
        <source>no device</source>
        <translation>kein Netzwerkgerät</translation>
    </message>
    <message>
        <location filename="airset.ui" line="72"/>
        <source>unknown state</source>
        <translation>unbekannter Status</translation>
    </message>
    <message>
        <location filename="airset.ui" line="429"/>
        <source>Managed networks</source>
        <translation>Verwaltete Netzwerke</translation>
    </message>
    <message>
        <location filename="airset.ui" line="487"/>
        <source>Available networks</source>
        <translation>Netzwerke in Reichweite</translation>
    </message>
    <message>
        <location filename="airset.ui" line="112"/>
        <source>Level: 0</source>
        <translation>Pegel: 0</translation>
    </message>
    <message>
        <location filename="airset.ui" line="119"/>
        <source>Noise: 0</source>
        <translation>Rauschen: 0</translation>
    </message>
    <message>
        <location filename="airset.ui" line="132"/>
        <source>no signal info</source>
        <translation>keine Signalinformationen</translation>
    </message>
    <message>
        <location filename="airset.ui" line="139"/>
        <source>Rate: 0Mb/s</source>
        <translation></translation>
    </message>
    <message>
        <location filename="airset.ui" line="146"/>
        <source>00:00:00:00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="airset.ui" line="153"/>
        <source>Quality: 0/0</source>
        <translation>Qualität: 0/0</translation>
    </message>
</context>
<context>
    <name>apconf</name>
    <message>
        <location filename="apconf.ui" line="14"/>
        <source>Configure Access Point</source>
        <translation>Netzwerkverbindung einstellen</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="22"/>
        <source>SSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="75"/>
        <source>Key management</source>
        <translation>Schlüsselverwaltung</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="88"/>
        <source>plain or WEP</source>
        <translation>unverschlüsselt oder WEP</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="93"/>
        <source>WPA-Personal (PSK)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="134"/>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="224"/>
        <source>Pre Shared Key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="232"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="255"/>
        <source>0 chars</source>
        <translation>0 Zeichen</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="274"/>
        <source>Show key as plain text</source>
        <translation>Zeige Schlüssel</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="295"/>
        <source>EAP Settings</source>
        <translation>EAP Einstellungen</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="303"/>
        <source>EAP method</source>
        <translation>EAP Methode</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="319"/>
        <source>MD5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="324"/>
        <source>TLS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="329"/>
        <source>MSCHAPV2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="334"/>
        <source>PEAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="339"/>
        <source>TTLS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="344"/>
        <source>GTC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="349"/>
        <source>OTP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="354"/>
        <source>LEAP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="377"/>
        <source>Identification</source>
        <translation>Identifikation</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="398"/>
        <source>Certificates</source>
        <translation>Zertifikate</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="404"/>
        <source>CA File</source>
        <translation>CA Datei</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="472"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="424"/>
        <source>Client File</source>
        <translation>Client Datei</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="453"/>
        <source>Private Key</source>
        <translation>Privater Schlüssel</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="459"/>
        <source>Key File</source>
        <translation>Schlüsseldatei</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="479"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="510"/>
        <source>WEP Keys</source>
        <translation>WEP Schlüssel</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="518"/>
        <source>WEP Key 0</source>
        <translation>WEP Schlüssel 0</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="548"/>
        <source>WEP Key 1</source>
        <translation>WEP Schlüssel 1</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="575"/>
        <source>WEP Key 2</source>
        <translation>WEP Schlüssel 2</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="602"/>
        <source>WEP Key 3</source>
        <translation>WEP Schlüssel 3</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="676"/>
        <source>Enable automatic selection</source>
        <translation>Automatische Auswahl dieses Zugangsknotens ermöglichen</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="619"/>
        <source>leave unchanged</source>
        <translation>unverändert lassen</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="612"/>
        <source>hex. chars</source>
        <translation>hex. Zeichen</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="42"/>
        <source>BSSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="55"/>
        <source>HH:HH:HH:HH:HH:HH; </source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="65"/>
        <source>any</source>
        <translation>jede</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="98"/>
        <source>WPA-Enterprise (IEEE802.1X/EAP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="106"/>
        <source>WPA2 (RSN)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="142"/>
        <source>General algorithm (group cipher)</source>
        <translation>Allgemeiner Algorithmus (Gruppenchiffre)</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="169"/>
        <source>Pairwise algorithm (pairwise cipher)</source>
        <translation>Pairwise Algorithmus (Pairwisechiffre)</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="177"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="182"/>
        <source>TKIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="187"/>
        <source>CCMP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="apconf.ui" line="669"/>
        <source>Enable SSID scanning (slower but needed if SSID is hidden)</source>
        <translation>Suche nach SSID (ist zwar langsamer, wird aber benötitgt, wenn die SSID versteckt wird)</translation>
    </message>
    <message>
        <location filename="apconf.ui" line="58"/>
        <source>00:00:00:00:00:00</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>connMan</name>
    <message>
        <location filename="connman.ui" line="14"/>
        <source>Connection Manager</source>
        <translation>Verbindungsmanager</translation>
    </message>
    <message>
        <location filename="connman.ui" line="57"/>
        <source>Main Toolbar</source>
        <translation>Hauptwerkzeugleiste</translation>
    </message>
    <message>
        <location filename="connman.ui" line="83"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="connman.ui" line="89"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="connman.ui" line="101"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="connman.ui" line="113"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="connman.ui" line="118"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location filename="connman.ui" line="123"/>
        <source>&amp;About QNUT</source>
        <translation>Über Q&amp;NUT</translation>
    </message>
    <message>
        <location filename="connman.ui" line="131"/>
        <source>Show &amp;balloon tips</source>
        <translation>Zeige Sprech&amp;blasen</translation>
    </message>
    <message>
        <location filename="connman.ui" line="139"/>
        <source>Show &amp;log</source>
        <translation>Zeige &amp;Protokoll</translation>
    </message>
    <message>
        <location filename="connman.ui" line="96"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
</context>
<context>
    <name>devdet</name>
    <message>
        <location filename="devdet.ui" line="14"/>
        <source>Device Details</source>
        <translation>Gerätedetails</translation>
    </message>
    <message>
        <location filename="devdet.ui" line="44"/>
        <source>unknown status</source>
        <translation>unbekannter Status</translation>
    </message>
    <message>
        <location filename="devdet.ui" line="64"/>
        <source>Show Details</source>
        <translation>Zeige Details</translation>
    </message>
    <message>
        <location filename="devdet.ui" line="117"/>
        <source>Show tray icon for this device</source>
        <translation>Zeige Symbol im Systembereich für dieses Gerät</translation>
    </message>
</context>
<context>
    <name>ipconf</name>
    <message>
        <location filename="ipconf.ui" line="20"/>
        <source>IP Configuration</source>
        <translation>IP Konfiguration</translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="28"/>
        <source>&amp;IP-Address:</source>
        <translation>&amp;IP-Adresse:</translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="129"/>
        <source>000.000.000.000;_</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="54"/>
        <source>&amp;Netmask:</source>
        <translation>&amp;Netzmaske:</translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="90"/>
        <source>&amp;Gateway:</source>
        <translation>&amp;Gateway:</translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="102"/>
        <source>DNS list</source>
        <translation>DNS Liste</translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="116"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="ipconf.ui" line="152"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>libnutclient::CDevice</name>
    <message>
        <location filename="../libnutclient/client.cpp" line="466"/>
        <source>Device properties fetched</source>
        <translation>Geräteeigenschaften geholt</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="472"/>
        <source>(%1) Error while retrieving dbus&apos; device information</source>
        <translation>(%1) Fehler beim Übertragen der Geräteinformationen über DBus</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="634"/>
        <source>(%1) Could not refresh device essid</source>
        <translation>(%1) Konnte die Geräte-Essid nicht aktualisieren</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="493"/>
        <source>(%2) wpa_supplicant config file at: %1</source>
        <translation>(%2) Pfad zur wpa_supplicant Konfigurationsdatei: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="496"/>
        <source>(%2) Error(%1) while retrieving device config</source>
        <translation>(%2) Fehler(%1) beim Übertragen der Gerätekonfiguration</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="502"/>
        <source>(%1) Error while trying to get environment list</source>
        <translation>(%1) Fehler beim Übertragen der Umgebungsliste</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="602"/>
        <source>(%1) Could not refresh environments</source>
        <translation>(%1) Die Umgebungen konnten nicht aktualisiert werden</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="622"/>
        <source>(%1) Could not refresh device properties</source>
        <translation>(%1) Die Geräteeigenschaften konnten nicht aktualisert werden</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="459"/>
        <source>Getting device properties at: %1</source>
        <translation>Hole Geräteeigenschaften: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="467"/>
        <source>Name : %1</source>
        <translation>Name: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="468"/>
        <source>Type: %1</source>
        <translation>Typ: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="469"/>
        <source>State: %1</source>
        <translation>Status: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="507"/>
        <source>Adding Environment at: %1</source>
        <translation>Füge Umbegung hinzu: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="526"/>
        <source>Active Environement: %1</source>
        <translation>Aktive Umbegung: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="612"/>
        <source>Refreshing active environment: %1</source>
        <translation>Aktualisiere aktive Umbegung: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="513"/>
        <source>(%1) Error while trying to add environment</source>
        <translation>(%1) Fehler beim Versuch eine Umgebung hinzuzufügen</translation>
    </message>
</context>
<context>
    <name>libnutclient::CDeviceManager</name>
    <message>
        <location filename="../libnutclient/client.cpp" line="133"/>
        <source>Please start nuts. Starting idle mode</source>
        <translation>Bitte NUTS starten. Gehe in Leerauf</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="250"/>
        <source>(%1) Failed to get DeviceList</source>
        <translation>(%1) Fehler beim Übertragen der Geräteliste</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="295"/>
        <source>NUTS has been started</source>
        <translation>NUTS wurde gestartet</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="309"/>
        <source>NUTS has been stopped</source>
        <translation>NUTS wurde gestartet</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="421"/>
        <source>(%1) Could not refresh device list</source>
        <translation>(%1) Die Geräteliste konnte nicht übertragen werden</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="439"/>
        <source>(%1) Error while retrieving device list</source>
        <translation>(%1) Fehler beim Übertragen der Geräteliste</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="328"/>
        <source>Adding device at: %1</source>
        <translation>Füge Gerät hinzu: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="119"/>
        <source>Error while trying to access the dbus service</source>
        <translation>Fehler beim Versuch auf den DBus-Dienst zuzugreifen</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="237"/>
        <source>You are not allowed to connect to nuts.</source>
        <translation>Sie sind nicht berechtigt sich mit NUTS zu verbinden.</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="238"/>
        <source>Please make sure you are in the correct group</source>
        <translation>Bitte stellen sie sicher, dass sie der richtigen Gruppe Mitglied sind</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="368"/>
        <source>The dbus daemon has been stopped. Please restart dbus and nuts</source>
        <translation>Der DBus-Dienst wurde angehalten. Bitte starten sie DBus</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="372"/>
        <source>dbus has been started initiating dbus interface</source>
        <translation>DBus wurde gestartet. Initialisiere DBus-Schnittstelle</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="120"/>
        <source>Please make sure that dbus is running</source>
        <translation>Bitte stellen sie sicher, dass DBus läuft</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="243"/>
        <source>Maybe you don&apos;t have sufficient rights</source>
        <translation>Möglicherweise haben sie nicht die benötigten Rechte</translation>
    </message>
</context>
<context>
    <name>libnutclient::CEnvironment</name>
    <message>
        <location filename="../libnutclient/client.cpp" line="862"/>
        <source>default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="867"/>
        <source>untitled (%1)</source>
        <translation>Unbenannt (%1)</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="806"/>
        <source>Error while retrieving environment properties</source>
        <translation>Fehler beim Übertragen der Umgebungseigenschaftem</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="841"/>
        <source>Error while retrieving environment&apos;s interfaces</source>
        <translation>Fehler beim Übertragen der Schnittstellen der Umgebung</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="875"/>
        <source>Error while refreshing environment properties</source>
        <translation>Fehler beim Aktualisieren der Umgebungseigenschaften</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="910"/>
        <source>Error while refreshing environment interfaces</source>
        <translation>Fehler beim Aktualisieren der Schnittstellen der Umgebung</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="971"/>
        <source>(%1) Error while trying to get SelectResult</source>
        <translation>(%1) Fehler beim Übertragen der Ergebnisses der Auswahlkriterien</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="989"/>
        <source>(%1) Error while trying to get SelectResults</source>
        <translation>(%1) Fehler beim Übertragen der Ergebnisse der Auswahlkriterien</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="814"/>
        <source>(%1) Error while retrieving environment config</source>
        <translation>(%1) Fehler beim Empfang der Umgebungskonfiguration</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="830"/>
        <source>(%1) Error while adding interfaces</source>
        <translation>(%1) Fehler beim Hinzufügen der Schnittstellen</translation>
    </message>
</context>
<context>
    <name>libnutclient::CInterface</name>
    <message>
        <location filename="../libnutclient/client.cpp" line="1060"/>
        <source>Interface state of %1 has changed to %2</source>
        <translation>Schnittstelle %1 änderte ihren Zustand nach %2</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="1016"/>
        <source>(%1) Error while retrieving interface properties</source>
        <translation>(%1) Fehler beim Empfang der Schnittstelleneigenschaften</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="1024"/>
        <source>(%1) Error while retrieving interface config</source>
        <translation>(%1) Fehler beim Empfang der Schnittstellenkonfiguration</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="1048"/>
        <source>Error while refreshing interface at: %1</source>
        <translation>Fehler beim Aktualisieren der Schnittstelle: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="1080"/>
        <source>Error while interface-&gt;needUserSetup at: %1</source>
        <translation>Fehler beim Aufruf &quot;interface-&gt;needUserSetup&quot;: %1</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="1098"/>
        <source>(%1) Error while setting user config at: %2</source>
        <translation>(%1) Fehler beim setzten der benutzerdefinierten Konfiguration: %2</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="1113"/>
        <source>Error while getting user config at: %1</source>
        <translation>Fehler beim Empfang der benutzerdefinierten Konfiguration: %1</translation>
    </message>
</context>
<context>
    <name>libnutclient::CLibNut</name>
    <message>
        <location filename="../libnutclient/client.cpp" line="75"/>
        <source>Please start NUTS</source>
        <translation>Bitte starten sie NUTS</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="79"/>
        <source>(%1)Error while setting-up dbusconnection</source>
        <translation>(%1) Fehler beim Erstellen der DBus-Verbindung</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="18"/>
        <source>up</source>
        <translation>verbunden</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="19"/>
        <source>unconfigured</source>
        <translation>unkonfiguriert</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="20"/>
        <source>got carrier</source>
        <translation>physikalisch verbunden</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="21"/>
        <source>activated</source>
        <translation>aktiviert</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="22"/>
        <source>deactivated</source>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="28"/>
        <source>Ethernet</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="29"/>
        <source>Wireless</source>
        <translation>Drahtlos</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="30"/>
        <source>PPP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="36"/>
        <source>off</source>
        <translation>aus</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="37"/>
        <source>static</source>
        <translation>statisch</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="38"/>
        <source>dynamic</source>
        <translation>dynamisch</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="39"/>
        <source>zeroconf</source>
        <translation>zerconf</translation>
    </message>
    <message>
        <location filename="../libnutclient/client.cpp" line="40"/>
        <source>wait for config</source>
        <translation>unkonfiguriert</translation>
    </message>
</context>
<context>
    <name>libnutwireless::CWpaSupplicant</name>
    <message>
        <location filename="../libnutwireless/wpa_supplicant.cpp" line="61"/>
        <source>auto-setting ap_scan=2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/wpa_supplicant.cpp" line="69"/>
        <source>Using your last ap_scan settings for auto-setting: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/wpa_supplicant.cpp" line="72"/>
        <source>You must set ap_scan to your needs!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>libnutwireless::CWpaSupplicantBase</name>
    <message>
        <location filename="../libnutwireless/base.cpp" line="90"/>
        <source>Error while trying to receive messages from wpa_supplicant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="175"/>
        <source>Could not open wpa_supplicant socket: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="199"/>
        <source>Could not open wpa_supplicant control interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="213"/>
        <source>Could not attach to wpa_supplicant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="232"/>
        <source>ERROR: Could not open socket to net kernel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="246"/>
        <source>wpa_supplicant connection established</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="280"/>
        <source>(%1)[%2] wpa_supplicant disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="483"/>
        <source>Warning, no timer present while trying to get scan results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="839"/>
        <source>(Wireless Extension) No device present</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="516"/>
        <source>(Wireless Extension) Device not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="575"/>
        <source>Range information are not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="625"/>
        <source>(%1) Failed to read scan data : %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="808"/>
        <source>No Scanresults available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="841"/>
        <source>(Wireless Extension) device not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="984"/>
        <source>Error occured while fetching wireless info: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="997"/>
        <source>Auto-resetting timer to 10 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="1003"/>
        <source>Cannot fetch wireless information as your wireless extension is too old</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libnutwireless/base.cpp" line="1004"/>
        <source>Think about updating your kernel (it&apos;s way too old)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qnut::CAccessPointConfig</name>
    <message>
        <location filename="accesspointconfig.cpp" line="490"/>
        <source>%1 chars</source>
        <translation>%1 Zeichen</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="524"/>
        <source>Select CA certificate file</source>
        <translation>CA Zertifikat auswählen</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="529"/>
        <source>Certificate files (%1)</source>
        <translation>Zertifikate (%1)</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="529"/>
        <source>Select client certificate file</source>
        <translation>Client Zertifikat auswählen</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="534"/>
        <source>Select key file</source>
        <translation>Schlüsseldatei auswählen</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="534"/>
        <source>Key files (%1)</source>
        <translation>Schlüsseldateien (%1)</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="416"/>
        <source>Error on applying settings</source>
        <translation>Fehler beim Setzen der Einstellungen</translation>
    </message>
    <message>
        <location filename="accesspointconfig.cpp" line="417"/>
        <source>WPA supplicant reported the following errors:</source>
        <translation>Der WPA Supplicant meldete folgende Fehler:</translation>
    </message>
</context>
<context>
    <name>qnut::CAdhocConfig</name>
    <message>
        <location filename="adhocconfig.cpp" line="140"/>
        <source>Error on applying settings</source>
        <translation>Fehler beim Setzen der Einstellungen</translation>
    </message>
    <message>
        <location filename="adhocconfig.cpp" line="141"/>
        <source>WPA supplicant reported the following errors:</source>
        <translation>Der WPA Supplicant meldete folgende Fehler:</translation>
    </message>
    <message>
        <location filename="adhocconfig.cpp" line="221"/>
        <source>Error reading ap config</source>
        <translation>Fehler beim Einlesen der Netzwerkkonfiguration</translation>
    </message>
    <message>
        <location filename="adhocconfig.cpp" line="216"/>
        <source>Unsupported group cipers retrieved: %1</source>
        <translation>Nichtunterstützte Gruppenchiffre: %1</translation>
    </message>
    <message>
        <location filename="adhocconfig.cpp" line="222"/>
        <source>Unsupported key management retrieved: %1</source>
        <translation>Nichtunterstützte Schlüsselverwaltung: %1</translation>
    </message>
</context>
<context>
    <name>qnut::CAvailableAPModel</name>
    <message>
        <location filename="availableapmodel.cpp" line="152"/>
        <source>undefined</source>
        <translation>undefiniert</translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="143"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="176"/>
        <source>SSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="180"/>
        <source>Key management</source>
        <translation>Schlüsselverwaltung</translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="182"/>
        <source>BSSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="81"/>
        <source>WEP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="84"/>
        <source>WPA PSK (ad-hoc)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="178"/>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="184"/>
        <source>Quality</source>
        <translation>Qualität</translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="186"/>
        <source>Level</source>
        <translation>Pegel</translation>
    </message>
    <message>
        <location filename="availableapmodel.cpp" line="188"/>
        <source>Encryption (general; pairwise)</source>
        <translation>Verschlüsselung (Allgemein; Pairwise)</translation>
    </message>
</context>
<context>
    <name>qnut::CConnectionManager</name>
    <message>
        <location filename="connectionmanager.cpp" line="41"/>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="54"/>
        <source>Cannot create/open log file.</source>
        <translation>Konnte Protokolldatei nicht öffnen/anlegen.</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="58"/>
        <source>%1 (v%2) started</source>
        <translation>%1 (v%2) gestartet</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="83"/>
        <source>&amp;Refresh devices</source>
        <translation>&amp;Geräte aktualisieren</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="84"/>
        <source>&amp;Enable</source>
        <translation>&amp;Aktivieren</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="85"/>
        <source>&amp;Disable</source>
        <translation>&amp;Deaktivieren</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="86"/>
        <source>&amp;Scripting settings...</source>
        <translation>&amp;Skripteinstellungen...</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="88"/>
        <source>&amp;Wireless settings...</source>
        <translation>Drahtlose &amp;Kommunikations-Einstellungen...</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="90"/>
        <source>&amp;Clear log</source>
        <translation>Protokoll &amp;leeren</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="193"/>
        <source>no devices present</source>
        <translation>keine Geräte vorhanden</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="276"/>
        <source>About QNUT</source>
        <translation>Über QNUT</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="290"/>
        <source>Log</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location filename="connectionmanager.cpp" line="54"/>
        <source>ERROR: %1</source>
        <translation>Fehler: %1</translation>
    </message>
</context>
<context>
    <name>qnut::CDeviceDetails</name>
    <message>
        <location filename="devicedetails.cpp" line="113"/>
        <source>&amp;Enable device</source>
        <translation>Gerät &amp;aktivieren</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="117"/>
        <source>&amp;Disable device</source>
        <translation>Gerät &amp;deaktivieren</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="133"/>
        <source>En&amp;vironments...</source>
        <translation>&amp;Umgebungen...</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="122"/>
        <source>&amp;Scripting settings...</source>
        <translation>&amp;Skripteinstellungen...</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="125"/>
        <source>&amp;Wireless settings...</source>
        <translation>Einstellungen der drahtlosen &amp;Kommunikation...</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="136"/>
        <source>E&amp;nter environment</source>
        <translation>&amp;Betrete Umgebung</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="140"/>
        <source>Set &amp;IP configuration...</source>
        <translation>Setzte &amp;IP-Konfiguration...</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="285"/>
        <source>QNUT - %1 ...</source>
        <translation>QNUT - %1 ...</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="274"/>
        <source>... is now up and running.</source>
        <translation>... ist jetzt verbunden.</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="282"/>
        <source>... is now activated an waits for carrier.</source>
        <translation>... ist jetzt aktiviert und wartet
auf eine physikalische Verbindung.</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="286"/>
        <source>... is now deactivated</source>
        <translation>... ist jetzt deaktiviert</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="307"/>
        <source>QNUT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="296"/>
        <source>%1 is now up and running.</source>
        <translation>%1 ist jetzt verbunden.</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="304"/>
        <source>%1 is now activated an waits for carrier.</source>
        <translation>%1 ist jetzt aktiviert und wartet
auf eine physikalische Verbindung.</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="308"/>
        <source>%1 is now deactivated</source>
        <translation>%1 ist jetzt deaktiviert</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="278"/>
        <source>... got carrier but needs configuration.

Klick here to open the device details.</source>
        <translation>... ist zwar physikalisch verbunden
hat aber unkonfigurierte Schnittellen.

Klicken sie hier, um die Details
dieses Gerätes anzuzeigen.</translation>
    </message>
    <message>
        <location filename="devicedetails.cpp" line="300"/>
        <source>%1 got carrier but needs configuration.

Klick here to open the device details.</source>
        <translation>%1 ist zwar physikalisch verbundenhat aber unkonfigurierte Schnittellen.Klicken sie hier, um die Detailsdieses Gerätes anzuzeigen.</translation>
    </message>
</context>
<context>
    <name>qnut::CEnvironmentDetailsModel</name>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="92"/>
        <source>not by user</source>
        <translation>automatisch</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="94"/>
        <source>not by arp</source>
        <translation>ARP nicht</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="96"/>
        <source>not by ssid</source>
        <translation>SSID nicht</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="98"/>
        <source>at least one not</source>
        <translation>mindestens eines nicht</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="100"/>
        <source>all not</source>
        <translation>alle nicht</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="107"/>
        <source>by user</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="109"/>
        <source>by arp</source>
        <translation>ARP</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="111"/>
        <source>by ssid</source>
        <translation>SSID</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="113"/>
        <source>all</source>
        <translation>alle</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="115"/>
        <source>at least one</source>
        <translation>mindestens eins</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="159"/>
        <source>Statement</source>
        <translation>Kriterium</translation>
    </message>
    <message>
        <location filename="environmentdetailsmodel.cpp" line="161"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>qnut::CEnvironmentTreeModel</name>
    <message>
        <location filename="environmenttreemodel.cpp" line="89"/>
        <source>active</source>
        <translation>aktiv</translation>
    </message>
    <message>
        <location filename="environmenttreemodel.cpp" line="107"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="environmenttreemodel.cpp" line="139"/>
        <source>Item</source>
        <translation>Objekt</translation>
    </message>
    <message>
        <location filename="environmenttreemodel.cpp" line="141"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="environmenttreemodel.cpp" line="143"/>
        <source>IP-Address</source>
        <translation>IP-Adresse</translation>
    </message>
</context>
<context>
    <name>qnut::CIPConfiguration</name>
    <message>
        <location filename="ipconfiguration.cpp" line="49"/>
        <source>Faild to add dns server</source>
        <translation>Fehler beim Hinzufügen des DNS-Server</translation>
    </message>
    <message>
        <location filename="ipconfiguration.cpp" line="49"/>
        <source>Address is invalid.</source>
        <translation>Adresse ist ungültig.</translation>
    </message>
</context>
<context>
    <name>qnut::CInterfaceDetailsModel</name>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="70"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="72"/>
        <source>IP-Address</source>
        <translation>IP-Adresse</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="74"/>
        <source>Netmask</source>
        <translation>Netzwerkmaske</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="76"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="78"/>
        <source>DNS sever #%1</source>
        <translation>DNS-Server #%1</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="116"/>
        <source>unknown</source>
        <translation>unbekannt</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="87"/>
        <source>user defined static</source>
        <translation>benutzerdefiniert statisch</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="94"/>
        <source>dynamic (DHCP)</source>
        <translation>dynamisch (DHCP)</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="91"/>
        <source>fallback: zeroconf</source>
        <translation>Ausweichlösung: zeroconf</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="93"/>
        <source>fallback: static</source>
        <translation>Ausweichlösung: statisch</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="105"/>
        <source>static</source>
        <translation>statisch</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="103"/>
        <source>static (fallback)</source>
        <translation>statisch (ausgewichen)</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="107"/>
        <source>dynamic</source>
        <translation>dynamic</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="110"/>
        <source>zeroconf (fallback)</source>
        <translation>zerconf (ausgewichen)</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="112"/>
        <source>zeroconf</source>
        <translation>zerconf</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="146"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="190"/>
        <source>Detail</source>
        <translation></translation>
    </message>
    <message>
        <location filename="interfacedetailsmodel.cpp" line="192"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>qnut::CManagedAPModel</name>
    <message>
        <location filename="managedapmodel.cpp" line="70"/>
        <source>any</source>
        <translation>jede</translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="104"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="106"/>
        <source>SSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="108"/>
        <source>BSSID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="77"/>
        <source>selected</source>
        <translation>ausgewählt</translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="79"/>
        <source>disabled</source>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="81"/>
        <source>enabled</source>
        <translation>auswählbar</translation>
    </message>
    <message>
        <location filename="managedapmodel.cpp" line="110"/>
        <source>status</source>
        <translation>Status</translation>
    </message>
</context>
<context>
    <name>qnut::COverViewModel</name>
    <message>
        <location filename="overviewmodel.cpp" line="128"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="166"/>
        <source>Device</source>
        <translation>Netzwerkgerät</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="168"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="170"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="174"/>
        <source>Environment</source>
        <translation>Umgebung</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="172"/>
        <source>IP-Address</source>
        <translation>IP-Adresse</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="176"/>
        <source>Network</source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="overviewmodel.cpp" line="140"/>
        <source>local</source>
        <translation>lokal</translation>
    </message>
</context>
<context>
    <name>qnut::CTrayIcon</name>
    <message>
        <location filename="trayicon.cpp" line="20"/>
        <source>Open Connection &amp;Manager</source>
        <translation>Öffne &amp;Verbindungsmanager</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="25"/>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="trayicon.cpp" line="17"/>
        <source>Network &amp;devices</source>
        <translation>Netzwerk &amp;Geräte</translation>
    </message>
</context>
<context>
    <name>qnut::CWirelessSettings</name>
    <message>
        <location filename="wirelesssettings.cpp" line="28"/>
        <source>Wireless Settings for &quot;%1&quot;</source>
        <translation>Einstellungen der drahtlosen Verbindung für &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="268"/>
        <source>Removing a managed network</source>
        <translation>Entferne ein verwaltetes Netzwerk</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="268"/>
        <source>Are you sure to remove &quot;%1&quot;?</source>
        <translation>Sind Sie sich sicher, dass sie &quot;%1&quot; entfernen möchten?</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="81"/>
        <source>&amp;Enable</source>
        <translation>&amp;Aktivieren</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="82"/>
        <source>Enable &amp;all</source>
        <translation>A&amp;lle Aktivieren</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="83"/>
        <source>&amp;Disable</source>
        <translation>&amp;Deaktivieren</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="84"/>
        <source>S&amp;witch</source>
        <translation>&amp;Wechseln</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="85"/>
        <source>&amp;Configure...</source>
        <translation>&amp;Konfigurieren...</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="90"/>
        <source>&amp;Remove</source>
        <translation>&amp;Entfernen</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="86"/>
        <source>Add &amp;network</source>
        <translation>&amp;Netzwerk hinzufügen</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="87"/>
        <source>Add ad-&amp;hoc</source>
        <translation>Ad-&amp;hoc Netz hinzufügen</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="89"/>
        <source>&amp;Save configuration</source>
        <translation>Konfiguration &amp;speichern</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="88"/>
        <source>Re&amp;load configuration</source>
        <translation>Konfiguration akt&amp;ualisieren</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="91"/>
        <source>Detailed &amp;view</source>
        <translation>De&amp;tailierte Ansicht</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="92"/>
        <source>Scan ne&amp;tworks</source>
        <translation>Ne&amp;tzwerke suchen</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="214"/>
        <source>Quality</source>
        <translation>Qualität</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="216"/>
        <source>Level</source>
        <translation>Pegel</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="217"/>
        <source>Noise</source>
        <translation>Rauschen</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="182"/>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="215"/>
        <source>Bitrate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="94"/>
        <source>Manage networks</source>
        <translation>Netzwerke verwalten</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="149"/>
        <source>Manage all...</source>
        <translation>Verwalten...</translation>
    </message>
    <message>
        <location filename="wirelesssettings.cpp" line="212"/>
        <source>no signal info</source>
        <translation>keine Signalinformationen</translation>
    </message>
</context>
<context>
    <name>qnut::QObject</name>
    <message>
        <location filename="common.h" line="48"/>
        <source>none</source>
        <translation>keine</translation>
    </message>
</context>
<context>
    <name>scrset</name>
    <message>
        <location filename="scrset.ui" line="14"/>
        <source>Scripting Settings</source>
        <translation>Skripteinstellungen</translation>
    </message>
    <message>
        <location filename="scrset.ui" line="20"/>
        <source>Execute scripts when device gets...</source>
        <translation>Führe Skiprte aus, wenn Geräte wechselt den Zustand nach ...</translation>
    </message>
    <message>
        <location filename="scrset.ui" line="26"/>
        <source>...deactivated</source>
        <translation>...deaktiviert</translation>
    </message>
    <message>
        <location filename="scrset.ui" line="33"/>
        <source>...activated</source>
        <translation>...aktiviert</translation>
    </message>
    <message>
        <location filename="scrset.ui" line="40"/>
        <source>...carrier</source>
        <translation>...physikalische Verbindung</translation>
    </message>
    <message>
        <location filename="scrset.ui" line="47"/>
        <source>...carrier and stays unconfigured</source>
        <translation>...physikalische Verbindung, bleibt aber konfigurationslos</translation>
    </message>
    <message>
        <location filename="scrset.ui" line="54"/>
        <source>...up</source>
        <translation>...verbunden</translation>
    </message>
</context>
</TS>
